/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import javax.swing.*;

public class NAND extends Bauteil{
    NAND(int id){
        super();
        this.id = id;
        setBounds(this.btnX + ((this.id % 7) * this.btnShift), this.btnY + ((this.id % 7) * this.btnShift), this.btnWidtht, this.btnHeight);
        setText("NAND " + this.id);
        System.out.println("Created NAND id: " + id);
        updateState();
    }
    public void updateState(){
        int countFalse = 0;
        for(int i = 0; i < inp.size(); i++){
            if(inp.get(i).state == false) countFalse++;
        }
        if(countFalse == inp.size()) this.state = true;
        else this.state = false;
        updateColor();
        updateFollower();
    }
}
